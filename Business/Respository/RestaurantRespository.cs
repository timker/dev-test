﻿using Business.DomainModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Respository
{
    public class RestaurantRespository : IRestaurantRespository
    {
        private string connectionString;
        public RestaurantRespository()
        {
            connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnection"].ConnectionString;
        }

        public List<Restaurant> GetRestaurants()
        {
            List<Restaurant> restaurants = new List<Restaurant>();

            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand("SELECT Id, Title, PhoneNumberText FROM Restaurant") { Connection = connection })
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var restaurant = RestaurantMapper(reader);
                            restaurants.Add(restaurant);
                        }
                    }
                }
            }

            return restaurants;   
        }

        public static Restaurant RestaurantMapper(IDataRecord record)
        {
            return new Restaurant()
            {
                Id = (int)record["Id"],
                Title = record["Title"] as string,
                PhoneNumberText = record["PhoneNumberText"] as string
            };
        }

        public Restaurant GetRestaurantById(int id)
        {
           // this is clearly not performant
           return GetRestaurants().Single(r => r.Id == id);
        }

        public void Update(Restaurant restaurant)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand("[UpdateRestaurant]") { Connection = connection })
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@id", restaurant.Id));
                    command.Parameters.Add(new SqlParameter("@title", restaurant.Title));
                    command.Parameters.Add(new SqlParameter("@phonenumberText", restaurant.PhoneNumberText));
                    
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
