﻿using Business.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Respository
{
    public interface IRestaurantRespository
    {

        List<Restaurant> GetRestaurants();

        Restaurant GetRestaurantById(int id);

        void Update(Restaurant restaurant);
    }
}
