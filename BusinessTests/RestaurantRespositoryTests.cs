﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Business.Respository;
using Moq;
using System.Data;

namespace BusinessTests
{
    [TestClass]
    public class RestaurantRespositoryTests
    {
        [TestMethod]
        [DeploymentItem("DatabaseTest.mdf")]
        public void GetRestaurants_RestaurantReturned()
        {
            RestaurantRespository rr = new RestaurantRespository();

            var result = rr.GetRestaurants();

            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        [DeploymentItem("DatabaseTest.mdf")]
        public void GetRestaurantById_ExistingData_Success()
        {
            RestaurantRespository rr = new RestaurantRespository();

            var result = rr.GetRestaurantById(381);

            Assert.AreEqual("Pulp Kitchen", result.Title);
            Assert.AreEqual("(02) 6257 4334", result.PhoneNumberText);
        }

        [TestMethod]
        public void RestaurantMapper_ValuesMappedCorrectly()
        {
            Mock<IDataRecord> dataRecordMock = new Mock<IDataRecord>();
            dataRecordMock.SetupGet(o => o["Id"]).Returns(99);
            dataRecordMock.SetupGet(o => o["Title"]).Returns("a title");
            dataRecordMock.SetupGet(o => o["PhoneNumberText"]).Returns("87 9879");

            var result = RestaurantRespository.RestaurantMapper(dataRecordMock.Object);

            Assert.AreEqual(99, result.Id);
            Assert.AreEqual("a title", result.Title);
            Assert.AreEqual("87 9879", result.PhoneNumberText);
        }
    }
}
