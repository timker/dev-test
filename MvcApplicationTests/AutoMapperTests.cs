﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MvcApplication.App_Start;

namespace MvcApplicationTests
{
    [TestClass]
    public class AutoMapperTests
    {
        [TestMethod]
        public void AssertConfigurationIsValid()
        {
            AutoMapperConfig.RegisterMaps();
            AutoMapper.Mapper.AssertConfigurationIsValid();
        }
    }
}
