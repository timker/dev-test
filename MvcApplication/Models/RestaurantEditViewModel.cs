﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MvcApplication.Models
{
    public class RestaurantEditViewModel
    {
        public int Id { get; set; }

        [DisplayName("Name")]
        public string Title { get; set; }

        public string PhoneNumberText { get; set; }
    }
}