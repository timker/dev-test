﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication.Models
{
    public class RestaurantIndexViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string PhoneNumberText { get; set; }
    }
}