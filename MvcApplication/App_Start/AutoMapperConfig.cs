﻿using Business.DomainModel;
using MvcApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication.App_Start
{
    public class AutoMapperConfig
    {
        public static void RegisterMaps()
        {
            AutoMapper.Mapper.CreateMap<Restaurant, RestaurantIndexViewModel>();
            AutoMapper.Mapper.CreateMap<Restaurant, RestaurantEditViewModel>();
        }
    }
}