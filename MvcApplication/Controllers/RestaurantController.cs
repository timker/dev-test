﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business.DomainModel;
using Business.Respository;
using AutoMapper;
using MvcApplication.Models;

namespace MvcApplication.Controllers
{
    public class RestaurantController : Controller
    {
        IRestaurantRespository restaurantRespository;

        public RestaurantController(IRestaurantRespository restaurantRespository)
        {
            this.restaurantRespository = restaurantRespository;
        }
        
        public ActionResult Index()
        {
            var restaurants = Mapper.Map<List<RestaurantIndexViewModel>>(restaurantRespository.GetRestaurants());
            return View(restaurants);
        }

        public ActionResult Edit(int id)
        {
           RestaurantEditViewModel r = Mapper.Map<RestaurantEditViewModel>(restaurantRespository.GetRestaurantById(id));
           return View(r);
        }

        [HttpPost]
        public ActionResult Edit(int id, RestaurantEditViewModel restaurant)
        {
            if(ModelState.IsValid)
            {
                var existingRestaurant = restaurantRespository.GetRestaurantById(id);
                existingRestaurant.Title = restaurant.Title;
                existingRestaurant.PhoneNumberText = restaurant.PhoneNumberText;

                restaurantRespository.Update(existingRestaurant);
                
                ViewBag.Message = String.Format("Saved '{0}'.", restaurant.Title);

            }

            return View(restaurant);
        }
    }
}
