﻿using AutoMapper;
using Business.Respository;
using MvcApplication.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication.Controllers
{
    public class HomeController : Controller
    {

        IRestaurantRespository restaurantRespository;

        public HomeController(IRestaurantRespository restaurantRespository)
        {
            this.restaurantRespository = restaurantRespository;
        }

        public ActionResult Index()
        {
            var restaurants = Mapper.Map<List<RestaurantIndexViewModel>>(restaurantRespository.GetRestaurants());
            return View(restaurants);
        }
        

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
